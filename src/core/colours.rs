use serenity::utils::Colour;

lazy_static! {
    pub static ref MAIN: Colour = Colour::new(0x5da9ff);
    pub static ref BLUE: Colour = Colour::new(0x6969ff);
    pub static ref RED: Colour = Colour::new(0xff4040);
    pub static ref GREEN: Colour = Colour::new(0x00ff7f);
}
